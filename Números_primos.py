#Soy Alejandro Delgado Alcaide.
n= int(input("Dame un número entero no negativo: "))

if n>0:
    print("Numeros primos iguales o menores: ")
    for i in range (2,n+1):
        creciente = 2
        primo = True
        while primo == True and creciente < i:
            if i % creciente == 0:
                primo = False
            else:
                creciente +=1
        if primo:
            print(f"{i}")
else:
    print("El número ingresado no es correcto.")

